(* ::Package:: *)

SetDirectory[NotebookDirectory[]];


<<IncreasingTrees.m


(*Construct some increasing trees*)
it5 = increasingTrees[{1,2,3,4,5}]


(*Calculate their kinematic dressings*)
dressings5 = itNumer[#,5]&/@it5


(*Sum them into a numerator.
Show that it is exactly what ptTreeNumer gives*)
num5 = Total[dressings5];
numer5 = ptTreeNumer[5,True][1,2,3,4,5];
ptCoef5 = Coefficient[ptTreeNumer[5,False],PT[{1,2,3,4,5}]];
num5 - numer5//Expand
ptCoef5 - num5//Expand


(*Assemble a color-ordered amplitude*)
co4 =   Total[mab[{1,2,3,4},{1,##,4}]*ptTreeNumer[4,True][1,##,4]&@@@Permutations[{2,3}]];
(*Make it pure gluons*)
co4 = co4/.W->WGluon;
(*This is what Aamp does for us*)
co4Dir = Coefficient[Aamp[4]/.W->WGluon,cf[1,2,3,4]];
ymsNorm[{1},{2},{3},{4}]co4 - co4Dir//Expand
(*The amplitude is gauge-invariant in all of the legs, using momentum conservation and transversality*)
Table[
	co4/.e[i]->k[i]/.k[4]->-k[1]-k[2]-k[3]/.d[e[4],k[3]]->d[e[4],-k[1]-k[2]]/.d[k[1],k[3]]->-d[k[1],k[2]]-d[k[2],k[3]]//Expand
,{i,1,4}]


(*Multi-trace numer gives expected results for single-trace*)
st6 = ptTreeNumer[6,True][1,2,3,4,5,6]/.W->WScalar;
mt6 = multiTraceNumer[{1,2,3,4,5,6},{{1,6}}];
st6 - mt6//Expand


(*Do a larger numerator*)
num8 = ptTreeNumer[8,True];//AbsoluteTiming
num8[1,2,3,4,5,6,7,8]/.W->WGluon//AbsoluteTiming



