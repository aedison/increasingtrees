(* ::Package:: *)

(***********************************************
*               IncreasingTrees
*          by Alex Edison and Fei Teng
*
*  A Mathematica package for building half-ladder
*numerators using the Cayley Tree/Increasing Tree
*algorithm.
***********************************************)


(* ::Section::Closed:: *)
(*Front Matter and Declarations*)


BeginPackage["IncreasingTrees`"]


$IncreasingTreesObjects={k,e,d,\[Chi],\[Xi],W,WL,WR,PT,spinChain,cf,ymCoup,scCoup};
$IncreasingTreesConstructors={increasingTrees,itNumer,ptTreeNumer,multiTraceNumer,multiTraceIntegrand,
								WScalar,WGluon,WFermion1,WFermion2,mab,Aamp,Mamp,ymsNorm};


(* ::Subsection::Closed:: *)
(*Constituent Objects that numerators are built from*)


k::usage="k[i_]: External momentum vector for particle i";
e::usage="e[i_]: External polarization vector for particle i";


d::usage="Lorentz product between two vectors. 
	Orderless, multilinear, and enforces transversality and onshellness of k and e.";


W::usage="Generic baseline function.  First and last label are singled out.
  Can be specialized to specific choices of particle";
WL::usage="Baseline function.  Differentiated for use in double copy";
WR::usage="Baseline function.  Differentiated for use in double copy";


PT::usage="Abstract Parke-Taylor factor";


spinChain::usage="Abstract spin-index contraction, bookended by fermionic wavefunctions.";


\[Chi]::usage="Fermionic wave function, described in Eqs.(2.8)-(2.10)";


\[Xi]::usage="Non-standard fermionic wave function, described in Eqs.(2.8)-(2.10)";


F::usage="F[i_]: linearized field strength for gluon i.  Only explicitly appears in fermion baselines.";


cf::usage="Generic color factor";


ymCoup::usage="Gauge boson coupling constant";
scCoup::usage="Biadjoint scalar coupling constant";


(* ::Subsection::Closed:: *)
(*Construction functions*)


increasingTrees::usage="increasingTrees[labels_List] builds an increasing tree with the given "<>
"labels, using the ordering they are supplied in.  "<>
"Returns a List of Graph objects.";


itNumer::usage="itNumer[g_Graph,one_,n_] builds numerator for a given increasingTree g, where "<>
"n is the label of the baseline endpoint";


ptTreeNumer::usage="ptTreeNumer[points_] builds the numerators for ALL increasing trees with given number of points.
	A second boolean argument is optional (default is False):
	----True:  returns a function that takes labels and returns the numerator for that half-ladder
	----False: returns ladder numerators for (1 \[Rho] n), \[Rho] \[Element] \!\(\*SubscriptBox[\(S\), \((n - 2)\)]\) dressed with PT factors";


multiTraceNumer::usage="multiTraceNumer[labels_List,traces_List] builds the multi-trace numerator "<>
"for the external particles given by labels, with trace groupings given by traces.  Pure gluons "<>
"are given by omitting the gluon label from the traces lists.";


multiTraceIntegrand::usage="multiTraceIntegrand[points_Integer,traces_List] builds ALL multi-traces numerators "<>
"for points number of external particles, grouped according to traces, and dressed with PT factors.";


mab::usage="mab[\[Alpha]_List,\[Beta]_List] builds the biadjoint scalar amplitude with partial orderings \[Alpha] and \[Beta]";


Aamp::usage="Aamp[n_Integer] assembles the n-point (super-)Yang-Mills amplitude from numerators, mab, and cf.
	Aamp[labels_Integer] uses the labels specified to build the amplitude.";


Mamp::usage="Mamp[n_Integer] assembles the n-point gravity integrand from numerators and mab.
	Mamp[labels_Integer] uses the labels specified to build the amplitude.";


(* ::Subsection::Closed:: *)
(*Evaluations of baseline function W*)


WScalar::usage="Baseline function where the special particles are scalars";
WGluon::usage="Baseline function where the special particles are gluons";
WFermion1::usage="WFermion1[x_][y__] evaluates the fermion baseline function with GP=(-1/2,-1/2), and gluon x is in the wrong ghost picture";
WFermion2::usage="Baseline function where the special particles are (-1/2, -3/2)-GP fermions";


(* ::Subsection::Closed:: *)
(*Amplitude Normalizations*)


ymsNorm::usage="ymsNorm[traces__List] computes the normalization given in (A.13) for the YMS half-integrand";


(* ::Section::Closed:: *)
(*Implementation*)


Begin["`Private`"];


(* ::Subsection::Closed:: *)
(*Define properties of k,e,d,W*)


k/:Abs[k[i_]]:=k[i]
k/:Sign[k[i_]]:=1


k[ii__]/;Length[{ii}]>1:=Total[k/@{ii}]


ClearAll[d];
SetAttributes[d,Orderless];
(*Linear in addition*)
d[x_,y_+z__]:= d[x,y] + d[x,Plus[z]];
(*Signs pull out*)
d[-x_,y_]:=-d[x,y];
(*Scalars pull out*)
d[b_ x_,y_]/;!MemberQ[{e,k},Head[b]]:=b d[x,y];
d[Plus[b__] x_,y_]:=d[Expand[Plus[b]x],y];
(*Onshell external*)
d[k[x_],k[x_]]:=0;
(*Transverse*)
d[k[x_],e[x_]]:=0;
d[0,__]:=0


(*The baseline functions are generalized products.  
Zeroing them out is good*)
ClearAll[W];
W[x___,0,y___]:=0


(* ::Subsection::Closed:: *)
(*increasingTrees - Construct all increasing trees consistent with an ordering*)


increasingTrees::argc="increasingTrees requires at least two vertices."


(*Allows forcing terms onto the baseline using head b*)
Clear[increasingTrees];
increasingTrees[orderedLabels_List]:=Block[
	{seed,lastB = First[orderedLabels]},
	If[Length[orderedLabels]===1,Message[increasingTrees::argc];Return[$Failed]];
	seed = {Graph[{DirectedEdge@@Reverse[orderedLabels[[1;;2]]]}]};
	If[Length[orderedLabels]>2,
		Do[
			If[Head[cv]===b,
				seed = Flatten[Table[
					EdgeAdd[g,DirectedEdge[cv,lastB]]
				,{g,seed}]];
				lastB = cv;
				,
				seed = Flatten[Table[
					EdgeAdd[g,DirectedEdge[cv,#]]&/@VertexList[g]
				,{g,seed}]]
			]
		,{cv,orderedLabels[[3;;]]}]
	];
	Graph[#,VertexLabels->"Name"]&/@seed
	
]


(* ::Subsection::Closed:: *)
(*vertexFactor - Recursively calculate a vertex factor for a given graph.  Not exposed to $ContextPath*)


(*Recursive construction of vertex factors for a graph*)
ClearAll[vertexFactor]
vertexFactor[g_Graph,v_,thewi_Association:<||>,thebeta_Association:<||>]:=vertexFactor[g,v,thewi,thebeta]=Block[
	{inV,vfs,totalLength,leafFactor,rootFactor,theai = First/@thewi,thebi=Last/@thewi},
	inV = Cases[EdgeList[g],DirectedEdge[x_,v]:>x];
	
	leafFactor = Switch[Head[v],
		WT, -1/Length[thewi[v[[1]]]]  k[theai[v[[1]]]],
		_,e[v]	
	];
	
	rootFactor[incV_]:= Switch[Head[v],
		WT,k@@Lookup[thebeta,Key[{v,incV}]],
		b,k[v[[1]]],
		_,k[v]
	];
	
	
	If[inV==={},
		(*We hit a leaf, which has length 1 and contributes leaf factor*)
		Return[ {leafFactor,1}]
	];
	
	(*Precompute the vertexFactors.  We need their chain lengths to get the combinatorics*)
	vfs = vertexFactor[g,#,thewi,thebeta]&/@inV;
	totalLength = Total[Last/@vfs];
	
	If[VertexOutDegree[g,v]===0,
		(*We start off the baseline, which just contributes a k*)
		Return[{  Times@@MapThread[d[rootFactor[#1],#2]&,{inV,(First/@vfs)}],totalLength+1}]];
		
	Switch[Head[v],
		WT,{ 1/(totalLength+1) (
			(*Stop-Restart term*)
			leafFactor Product[ d[vfs[[i,1]], rootFactor[inV[[i]]] ] ,{i,Length[vfs]}] + 
			(*Through term*)
			Sum[ 
				(*Throughput piece*)
				If[Lookup[thebeta,Key[{v,inV[[i]]}],{}]===thewi[v[[1]]],
					(*Valid throughput ordering*)
					Length[thewi[v[[1]]]] leafFactor vfs[[i,2]] d[vfs[[i,1]],k[thewi[v[[1]] ][[-1]]] ] *
						(*The remaining Stop-restart pieces*)
						Product[ d[vfs[[j,1]],rootFactor[inV[[j]]]],{j,Complement[Range[Length[vfs]],{i}]}]
					,
					(*Invalid throughput ordering*)
					0
				]
				,{i,Length[vfs]}]
		 ) ,totalLength+1},
		_,{(e[v]  Product[ d[vf[[1]],k[v]],{vf,vfs}] -
			 1/(1+totalLength) k[v] Sum[ vfs[[i,2]] d[vfs[[i,1]],e[v]] Product[ d[k[v],vfs[[vj,1]]],{vj,Complement[Range[Length[vfs]],{i}]}],{i,Length[vfs]}])
			,totalLength+1}
	]
]


(* ::Subsection::Closed:: *)
(*itNumer - Build the contribution from an Increasing Tree by applying vertexFactor to each root along the baseline and inserts W*)


itNumer::BadBaseline="`1` is not a valid baseline endpoint of `2`.";


itNumer[g_Graph,maxV_]:=Block[
	{minV,vl,baseline,baselineFactor,baselineEdges,
	remainingGraph,rgel,rgvl,interestingVertices,results},
	
	vl = VertexList[g]//Sort;
	
	If[VertexInDegree[g,maxV]>0||!MemberQ[vl,maxV],Message[itNumer::BadBaseline,maxV,g];Return[$Failed]];
	
	minV = Select[vl,VertexOutDegree[g,#]==0&,1][[1]];
	baseline = FindPath[g,maxV,minV][[1]]//Reverse;
	
	baselineFactor = W@@baseline;
	
	baselineEdges = PathGraph[baseline//Reverse,DirectedEdges->True]//EdgeList;
	
	remainingGraph = EdgeDelete[g,baselineEdges];
	rgel = EdgeList[remainingGraph];
	rgvl = VertexList[remainingGraph];
		
	interestingVertices = Select[baseline,VertexInDegree[remainingGraph,#]>0&];
	
	results = vertexFactor[remainingGraph,#]&/@interestingVertices;
	
	baselineFactor * Times@@(First/@results)
	
	
]


(* ::Subsection::Closed:: *)
(*ptTreeNumer - Build full ladder numerators*)


Clear[ptTreeNumer];
ptTreeNumer[numExternals_Integer,onlyFuncQ_:False]:=Block[
	{funcVars,baseOrdering,theFunc},
	funcVars = ToExpression["x"<>ToString[#]]&/@Range[numExternals];
	baseOrdering = Total[itNumer[#,funcVars[[-1]]]&/@increasingTrees[funcVars]]//ExpandAll;
	theFunc = With[{body=baseOrdering},
		Function[Evaluate[funcVars],body]];
	
	If[onlyFuncQ,
		theFunc
		,
		Total[(PT[{##}]theFunc[##])&@@@(({1,##,numExternals}&@@@Permutations[Range[2,numExternals-1]]))]
	]
]


(* ::Subsection::Closed:: *)
(*Multi-Trace helpers.  Not exposed to $ContextPath*)


(* ::Subsubsection::Closed:: *)
(*Shuffle Product and KK*)


(*Shuffle product between two lists*)
shuffleW[s1_, s2_] := 
  Module[{p, tp, ord},
    p = Permutations @ Join[1 & /@ s1, 0 & /@ s2]\[Transpose];
    tp = BitXor[p, 1];
    ord = Accumulate[p] p + (Accumulate[tp] + Length[s1]) tp;
    Transpose[Outer[Part, {Join[s1, s2]}, ord, 1][[1]]]
  ]
shuffleW[{},{}]:={{}}


(*KK a PT factor such that left becomes the left-most, and right the right-most*)
KKify[PT[y_List],left_,right_]:=Block[
	{activeList,pi,rP,kkAlpha,kkBeta},
	If[!MemberQ[y,left]||!MemberQ[y,right],Return[0]];
	activeList=y;
	While[activeList[[1]]=!=left,activeList = RotateLeft[activeList]];
	pi = First/@PositionIndex[activeList];
	rP = pi[right];
	kkAlpha = activeList[[2;;rP-1]];
	If[rP=!=Length[activeList],
		kkBeta = activeList[[rP+1;;]]
		,
		kkBeta = {}
	];
	(-1)^(Length[kkBeta]) Sum[ PT[Flatten[{left,ss,right}]],{ss,shuffleW[kkAlpha,Reverse[kkBeta]]}]
]


(*Compare two lists of traces and see if they are in the KK of each other, returning the signs of each*)
kkCompare[bt_List,ot_List]:=Times@@MapThread[Coefficient[KKify[PT[#1],#2[[1]],#2[[-1]]],PT[#2]]&,{bt,ot}]


(* ::Subsubsection::Closed:: *)
(*Evaluate a multi-trace tree where 1 and n are already in the same trace.  Not exposed to $ContextPath*)


(*Gets called by multiTraceNumer after it has ensured that everything is lined up*)
multiTraceSameBase[labels_List,allTraces_List]:= Block[
	{traces,labelsPI,tracesOrdered,orderedTraces,extraSign,wi,posMapping,vertexOrdering,
	\[Beta]\[Beta],beta,theTrees,interestingParts,validRoots},
	
	(*Gluons are Length-1 traces*)
	traces = Select[allTraces,Length[#]>1&];
	
	labelsPI = First/@PositionIndex[labels];
	tracesOrdered = SortBy[{!IntersectingQ[#,labels[[{1,-1}]]]&,Min[labelsPI/@#]&}]@traces;
	
	orderedTraces = Function[{ll},Select[labels,MemberQ[ll,#]&]]/@tracesOrdered;
	(*Need to do a KK check, only if the traces aren't already aligned to the labels*)
	If[orderedTraces=!=tracesOrdered,
		extraSign = kkCompare[orderedTraces,tracesOrdered];
		(*There is a misalignment!  So bail out immediately*)
		If[extraSign===0,Return[0]];
		,
		extraSign=1;
	];
	
	
	(*Mapping into labeled subtraces*)
	wi = Association[MapThread[#1->#2&,{Range[Length[#]],#}]&@(orderedTraces)];

	posMapping = Function[{ll},If[#==={},ll->ll,
					If[First[First[#]]=!=1,
						ll->WT[First[First[#]]]
						,
						ll->b[ll]]
					]&@Position[tracesOrdered,ll]]/@labels;
	
	vertexOrdering = DeleteDuplicates[labels/.posMapping];
	
	\[Beta]\[Beta][WT[x_],WT[y_]]:=  TakeWhile[Select[labels,MemberQ[wi[x],#]||MemberQ[wi[y],#]&],MemberQ[wi[x],#]&];
	\[Beta]\[Beta][WT[x_],y_Integer]:=  TakeWhile[Select[labels,MemberQ[wi[x],#]||#==y&],MemberQ[wi[x],#]&];
	
	beta = Association[Table[
		If[Head[vertexOrdering[[i]]]=!=b&&Head[vertexOrdering[[j]]]=!=b,
		{vertexOrdering[[i]],vertexOrdering[[j]]}->\[Beta]\[Beta][vertexOrdering[[i]],vertexOrdering[[j]]]
		,
		Nothing
		]
	,{i,Length[#]-1},{j,i+1,Length[#]}]]&@vertexOrdering;
	
	theTrees = increasingTrees[vertexOrdering];
	
	interestingParts = Function[{g},VertexDelete[g,Select[VertexList[g],VertexDegree[g,#]==0&]]]/@(EdgeDelete[#,DirectedEdge[b[_],b[_]]]&/@theTrees);
	
	validRoots = Select[VertexList[#],Head[#]==b&]&/@interestingParts;
	
	extraSign Total[MapThread[  Product[vertexFactor[#1,vr,wi,beta]//First,{vr,#2}]&,{interestingParts,validRoots}]]
	
]


(* ::Subsection::Closed:: *)
(*multiTraceNumer - Construct the multi-trace numerator with the given external ordering and traces.  *)
(*	Gluons should be signaled by leaving the gluon*)
(*	 label out of the traces list*)


multiTraceNumer::BadTraces="Traces include the elements `1`, which are not particle labels in `2`.";


multiTraceNumer[labels_List,allTraces_List]:=Block[
	{traces,labelOrderedTraces,baseTraces,overallSign,theGlue,w1,wM,toPartition,
	partitionings,partitionedOrderedTraces,sendingDown,wPrefactor},
	
	If[#=!={},Message[multiTraceNumer::BadTraces,#,labels];Return[$Failed]]&@Complement[Flatten[allTraces],labels];
	
	(*Allows gluons as explicit Length-1 traces*)
	traces = Select[allTraces,Length[#]>1&];
	
	(*Write the traces in the order of the labels*)
	labelOrderedTraces = Function[{ll},Select[labels,MemberQ[ll,#]&]]/@traces;

	labelOrderedTraces = SortBy[labelOrderedTraces,Position[labels,#[[1]] ]&];
	baseTraces = Permute[traces,FindPermutation[Sort/@traces,Sort/@labelOrderedTraces]];

	(*KK check everyone participating*)
	overallSign = kkCompare[baseTraces,labelOrderedTraces];

	
	(*If any of the KKs are misaligned, contributes 0*)
	If[overallSign===0,Return[0]];
	
	(*The gluons need to ride along at this point to be possibly shoved into
		the baseline*)
	theGlue = Complement[labels,Flatten[labelOrderedTraces]];
	labelOrderedTraces = Join[labelOrderedTraces,List/@theGlue];
	labelOrderedTraces = SortBy[labelOrderedTraces,Position[labels,#[[1]] ]&];
	
	(*Pick out first and last traces*)
	w1 = Select[labelOrderedTraces,MemberQ[#,labels[[1]]]&][[1]];
	wM = Select[labelOrderedTraces,MemberQ[#,labels[[-1]]]&][[1]];
	
	(*If the baseline is already complete, we have no more work to do here*)
	If[w1===wM,Return[overallSign*multiTraceSameBase[labels,labelOrderedTraces/.{x_}:>Nothing]]];
	
	toPartition = Complement[Range[Length[labelOrderedTraces]],Flatten[{Position[labelOrderedTraces,w1],Position[labelOrderedTraces,wM]}]];
	

	While[w1[[1]]=!=labels[[1]],w1 = RotateLeft[w1]];
	While[wM[[-1]]=!=labels[[-1]],wM = RotateLeft[wM]];

	
	(*Set up A U B = labels. 
	We may need to foribly keep at least one trace in A*)
	partitionings = {#,Complement[toPartition,#]}&/@Subsets[toPartition];
	

	partitionedOrderedTraces = Join[{w1},labelOrderedTraces[[#[[1]]]],{wM}]&/@partitionings;
	sendingDown = (labelOrderedTraces[[#]]/.{{x_Integer}:>Nothing})&/@partitionings[[All,2]];
	wPrefactor = WMT@@@partitionedOrderedTraces;
	

	
	overallSign Total[MapThread[#3 multiTraceSameBase[labels,Join[{Flatten[#1]},#2]] & ,
							{partitionedOrderedTraces,sendingDown,wPrefactor}]]
	
	
	
]


(* ::Subsection::Closed:: *)
(*multiTraceIntegrand - Do the permutation sum and multiply in the relevant PT factors*)


multiTraceIntegrand::BadTraces="Traces include the elements `1`, which are not particle labels in Range[`2`].";


multiTraceIntegrand[numPoints_Integer,traces_List]:=Block[
	{},
	If[#=!={},Message[multiTraceIntegrand::BadTraces,#,numPoints];Return[$Failed]]&@Complement[Flatten[traces],Range[numPoints]];
	Total[ 
		PT[{1,##,numPoints}] multiTraceNumer[{1,##,numPoints},traces]&@@@Permutations[Range[2,numPoints-1]]]
]


(* ::Subsection::Closed:: *)
(*Various versions of W*)


(*Include the (-1)^(numGluons) explicitly here*)
(*Note that we don't actually expose this, since the signs don't line up precisely with what we state in the paper:
	the KK induced signs are actually handled in the numerator code, since that is where we have access to both the
	traces and label orderings.  Here we just handled the gluon counting.*)
WMT[y__List]:=((-1)^Count[{y},{_Integer},Infinity])Total[Flatten[Outer[Times@@FoldPairList[ {d[Last[#1],First[#2]],#2}&,{##}]&,##,1]&@@(
				Table[
					If[Length[{y}[[i]]]===1,
						Switch[i,
							1,{{e@@{y}[[i]]}},
							Length[{y}],{{e@@{y}[[i]]}},
							_,{{k@@#,e@@#},{-e@@#,k@@#}}&@{y}[[i]]]
					,
						Switch[i,
							Length[{y}],{{k[First[{y}[[i]]]]}},
							_,{{k[First[#]],-k[Last[#]]}}&@{y}[[i]]]
					],{i,Length[{y}]}])]]
WMT[y_List]:=1


(*The gluon baseline is just the MT baseline, with no scalars*)
WGluon[y__Integer]:=WMT@@(List/@{y})


(*This is specialized for a single pair of scalars*)
WScalar[x_,y__,z_]:=0
WScalar[x_,z_]:=1


WFermion1[x_][a_,b___,x_,c___,d_]:=1/2 (-1)^Length[{b,x,c}]spinChain[\[Chi][a],Sequence@@(F/@{b}),e[x],Sequence@@(F/@{c}),\[Chi][d]]
WFermion1[x_][a_,b___,d_]/;!MemberQ[{b},x]:=0


WFermion2[x_,y___,z_]:=(-1)^Length[{y}]spinChain[\[Chi][x],Sequence@@(F/@{y}),\[Xi][z]]


(* ::Subsection::Closed:: *)
(*Amplitude Assembly *)


(* ::Subsubsection::Closed:: *)
(*Normalization*)


ymsNorm[traces__List]:=Block[
	{glueCount,honestTraces,traceCount,scalarCount},
	glueCount = Length[Select[{traces},Length[#]==1&]];
	honestTraces = Select[{traces},Length[#]>1&];
	traceCount = Length[honestTraces];
	scalarCount = Length[Flatten[honestTraces]];
	
	(-1)^(traceCount)(-2I) (ymCoup/Sqrt[2])^(glueCount+2*traceCount-2) (-scCoup/4)^(scalarCount-2traceCount)
]


(* ::Subsubsection::Closed:: *)
(*Helper Functions*)


orderedSplittings[toSplit_List]:=Table[  {toSplit[[;;i]],toSplit[[i+1;;]]}       ,{i,1,Length[toSplit]-1}]


ClearAll[jjj]
jjj[{i_}, {j_}] := If[i===j,1,0]
jjj[\[Alpha]_List, \[Beta]_List] /; Sort[\[Alpha]] == Sort[\[Beta]] :=
 1/(ss @@ \[Alpha]) Sum[
   jjj[l[[1]], r[[1]]] jjj[l[[2]], r[[2]]] -
    jjj[l[[1]], r[[2]]] jjj[l[[2]], r[[1]]], 
    {l,orderedSplittings[\[Alpha]]}, {r,orderedSplittings[\[Beta]]}]
jjj[__] := 0


ss[i__]:=d[k[i],k[i]]


Clear[sss]
sss[y__,x_List]:=(sss@@x)sss[Sequence@@Flatten[x],y]
SetAttributes[sss,Orderless]


descSign[xx_List]:=Times@@Sign[Differences[xx]]


descSignShift[a_,b_]:=Block[{shift=0,theShifts},
	theShifts = MapThread[ If[#1<#2,shift+=(#2-#1+1);#1->#1+shift,#1->#1+shift]&,{Rest[a],Most[a]}];
	descSign[b/.theShifts]
]


mabTab[{first_,mid___,last_}]:=Block[
	{xV,allDensFunc,mabFunc,posInd},
	xV = Table[ToExpression["$$x"<>ToString[i]],{i,Length[{first,mid}]}];
	allDensFunc=With[{xVars = xV,theExpr = sss@@@Groupings[xV,2]/.sss@@xV->1},
			Function[Evaluate[xVars],theExpr]];
	
	posInd = First/@PositionIndex[{first,mid,last}];		
	mabFunc = descSignShift[posInd/@#1,posInd/@#2]Total[1/Intersection[allDensFunc@@#1,allDensFunc@@#2]]&;
			
	Outer[mabFunc,{first,##}&@@@Permutations[{mid}],{first,##}&@@@Permutations[{mid}],1]/.sss->ss
]


(* ::Subsubsection::Closed:: *)
(*mab*)


mab::argkk = "Particle orderings `1` and `2` are not in the same KK basis";


mab[\[Alpha]_List,\[Beta]_List]:=If[\[Alpha][[1]]=!=\[Beta][[1]]||\[Alpha][[-1]]=!=\[Beta][[-1]],
	Message[mab::argkk,\[Alpha],\[Beta]];Return[$Failed]
	,
	Expand[(ss @@ Most[\[Alpha]]) jjj[Most[\[Alpha]], Most[\[Beta]]]]
]


(* ::Subsubsection::Closed:: *)
(*Aamp*)


Aamp[labels_List]/;Length[labels]>2&&DuplicateFreeQ[labels]:=Block[
{ptFunc,numerFunction,n=Length[labels],permList,numerVec,cfVec,mabT},
	permList = {labels[[1]],##,labels[[-1]]}&@@@Permutations[labels[[2;;-2]]];
	ptFunc = ptTreeNumer[n,True];
	numerVec = ptFunc@@@permList;
	cfVec = cf@@@permList;
	mabT = mabTab[labels];
	
	(ymsNorm@@(List/@Range[n])) cfVec.mabT.numerVec
]


Aamp[n_Integer]/;n>2:=Aamp[Range[n]]


(* ::Subsubsection::Closed:: *)
(*Mamp*)


Mamp[labels_List]/;Length[labels]>2&&DuplicateFreeQ[labels]:=Block[
{ptFunc,numerFunction,n=Length[labels],permList,numerVec,cfVec,mabT},
	permList = {labels[[1]],##,labels[[-1]]}&@@@Permutations[labels[[2;;-2]]];
	ptFunc = ptTreeNumer[n,True];
	numerVec = ptFunc@@@permList;
	mabT = mabTab[labels];
	
	(numerVec/.W->WL).mabT.(numerVec/.W->WR)
]


Mamp[n_Integer]/;n>2:=Mamp[Range[n]]


(* ::Section::Closed:: *)
(*End Matter*)


End[]
EndPackage[]


(* ::Section::Closed:: *)
(*Welcome String*)


"            IncreasingTrees
A Mathematica package by Alex Edison and Fei Teng for
building half-ladder numerators using the spanning tree
algorithm.

---$IncreasingTreesObjects lists the names of various
objects that appear in the results of constructions.
---$IncreasingTreesConstructors lists the names of all public
functions used for building numerators.
Brief descriptions of all Symbols used are given by ?symbname.
This welcome message can be suppressed by adding a ; at the end
of the Get or << command that loaded the package."
